package in.mukund

import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration


object Main extends App {

  class Test(tag: Tag) extends Table[(Int, String)](tag, "TEST") {
    def id = column[Int]("ID", O.PrimaryKey)
    def text = column[String]("TEXT")

    def * = (id, text)
  }

  val test = TableQuery[Test]

  val db = Database.forConfig("mydb")

  val setup = DBIO.seq(
    // Create the tables, including primary and foreign keys
    (test.schema).create
  )

  val insertData = DBIO.seq(
    test ++= Seq(
      (1, "Sample text"),
      (2, "Some other text")
    )
  )

  try {
    val resultFuture = db.run(insertData)
    Await.result(resultFuture, Duration.Inf)
    // lines.foreach(Predef.println _)
  } finally db.close
}
