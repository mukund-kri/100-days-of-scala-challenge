import Dependencies._

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization := "in.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).
  settings(
    name := "Slick",

    // TODO: Add your dependency here\
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % "3.3.3",
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "org.postgresql" % "postgresql" % "42.2.5"
    ),
    
    libraryDependencies ++= Seq(
      scalaTest % Test
      // TODO: Add your test dependencies here
    )
  )
