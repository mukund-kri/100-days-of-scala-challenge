// THIS PROGRAM DOES NOT WORK AS SLICK DOES NOT SUPPORT SCALA 3 YET.
package in.mukund

import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration


class Test(tag: Tag) extends Table[(Int, String)](tag, "TEST"):
  def id = column[Int]("ID", O.PrimaryKey)
  def text = column[String]("TEXT")

  def * = (id, text)


object Main:
  def main(args: Array[String]) =
    println("Hello, World -- new syntax. In object")

    val test = TableQuery[Test]
    val db = Database.forConfig("mydb")
