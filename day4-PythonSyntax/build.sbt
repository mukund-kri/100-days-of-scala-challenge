import Dependencies._

ThisBuild / scalaVersion := "3.0.2"
ThisBuild / organization := "in.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).
  settings(
    name := "PythonSyntax",

    libraryDependencies += ("com.typesafe.slick" %% "slick" % "3.3.3").cross(CrossVersion.for3Use2_13)
  )
