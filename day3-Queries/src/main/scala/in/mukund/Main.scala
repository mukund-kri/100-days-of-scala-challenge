package in.mukund

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global

import com.github.tototoshi.csv.CSVReader
import javax.xml.crypto.Data
import scala.util.{Success, Failure}


object Main extends App {

  // val reader = CSVReader.open("data/mca.csv")

  val db = Database.forConfig("company_master")
  
  try {

    /*
    val q = Models.company.filter(_.registrationYear === 2015)
    val result = q.result
    db.run(result).foreach(println(_))
    */

    // Using the for interpolation

    val q1 = (for {
      c <- Models.company if c.registrationYear === 2015
    } yield c).groupBy(_.pba)
    
    val q2 = q1.map {
      case (pba, rest) => (pba, rest.length)
    }
    val r1 = q2.result
    val f = db.run(r1)

    f.onComplete { 
      case Success(s) => s.foreach(println)
      case Failure(e) => println(e)
    }
    
  } finally db.close
  
}

