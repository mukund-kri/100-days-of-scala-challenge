package in.mukund

import slick.jdbc.PostgresProfile.api._


object Models {

  class Company(tag: Tag) extends Table[(String, Int, Int, String)](tag, "company") {
    def cin = column[String]("cin")
    def registrationYear = column[Int]("registration_year")
    def authorizedCap = column[Int]("authorized_cap")
    def pba = column[String]("pba")

    def * = (cin, registrationYear, authorizedCap, pba)
  }

  val company = TableQuery[Company]

  val setup = DBIO.seq(
    (company.schema).createIfNotExists
  )
}