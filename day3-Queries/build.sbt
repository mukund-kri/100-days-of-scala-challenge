import Dependencies._

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization := "in.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).
  settings(
    name := "Loader",

    // Database access with slick
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % "3.3.3",
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "org.postgresql" % "postgresql" % "42.2.5"
    ),

    // Reading CSV
    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.8",

    
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.9"
      // TODO: Add your test dependencies here
    )
  )
