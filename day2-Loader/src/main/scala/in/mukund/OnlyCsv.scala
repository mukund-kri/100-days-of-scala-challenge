package in.mukund

import com.github.tototoshi.csv._


object OnlyCsv extends App:

  try
    // Read in the CSV data
    val reader = CSVReader.open("data/mca.csv")

    val requiredData = reader
      .allWithHeaders()
      .filter(_("DATE_OF_REGISTRATION") != "NA")
      .map(row => (
        row("CORPORATE_IDENTIFICATION_NUMBER"),  
        row("DATE_OF_REGISTRATION").takeRight(4).toInt,
        row("AUTHORIZED_CAP").toFloat.toInt,
        row("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")
      ))
      .toSeq
  
    reader.close()

    // print all the records to console
    requiredData.foreach(println)

  catch
    case e: Exception => println(e)