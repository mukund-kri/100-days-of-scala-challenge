// src/scripts/scala/Upper1.scala

class Upper1:

  def convert(strings: Seq[String]): Seq[String] =
    strings.map((s: String) => s.toUpperCase)
    

val up = new Upper1()
val uppers = up.convert(List("Hello", "World!"))
print(uppers)