package in.mukund


object HelloUpper1:

  // Entry point type 1
  def main(params: Array[String]): Unit =
    println("running HelloUpper1.main")
    params.map(s => s.toUpperCase).foreach(s => printf("%s ", s))
    println()

// Entry point type 2. Main function in top level
def main(params: Array[String]): Unit =
  println("Running toplevel main")
  params.map(s => s.toUpperCase).foreach(s => printf("%s ", s))
  println()

// Entry point type 3. Using type annotation
@main def Hello(params: String*): Unit =
  println("Using @main annotation")
  params.map(s => s.toUpperCase).foreach(s => printf("%s ", s))
  println()
